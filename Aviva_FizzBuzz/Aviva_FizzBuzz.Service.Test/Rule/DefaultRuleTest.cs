﻿using System;
using NUnit.Framework;
using Moq;
using FluentAssertions;
using Aviva_FizzBuzz.Service.Rule;

namespace Aviva_FizzBuzz.Service.Test.Rule
{
    [TestFixture]
    public class DefaultRuleTest
    {
        private DefaultRule defaultRule;

        [SetUp]
        public void SetUpTests()
        {
            this.defaultRule = new DefaultRule();
        }

        [Test]
        public void ModCheck_ShouldReturnTrue_WhenNumberIsNotDivisibleByThreeAndFive()
        {
            bool result;

            result = this.defaultRule.ModCheck(1);

            result.Should().Be(true);
        }

        [Test]
        public void Print_ShouldReturnBuzz_WhenModCheckReturnsTrue()
        {
            string output;

            output = this.defaultRule.Print(1, DayOfWeek.Monday);

            output.Should().Be("1");
        }

    }
}
