﻿using System;
using NUnit.Framework;
using Moq;
using FluentAssertions;
using Aviva_FizzBuzz.Service.Rule;

namespace Aviva_FizzBuzz.Service.Test.Rule
{
    [TestFixture]
    public class BuzzRuleTest
    {
        private BuzzRule buzzRule;

        [SetUp]
        public void SetUpTests()
        {
            buzzRule = new BuzzRule();
        }

        [Test]
        public void ModCheck_ShouldReturnTrue_WhenNumberIsDivisibleByFive()
        {
            bool result;

            result = this.buzzRule.ModCheck(5);

            result.Should().Be(true);
        }

        /// <summary>
        /// Print method should return buzz when called.
        /// </summary>
        [Test]
        public void Print_ShouldReturnBuzz_WhenModCheckReturnsTrue()
        {
            string output;

            output = this.buzzRule.Print(5, DayOfWeek.Monday);

            output.Should().Be("buzz");
        }

        [Test]
        public void Print_ShouldReturnWizz_WhenDayOfWeekIsWednesday()
        {
            string output;

            output = this.buzzRule.Print(5, DayOfWeek.Wednesday);

            output.Should().Be("wuzz");
        }
    }
}
