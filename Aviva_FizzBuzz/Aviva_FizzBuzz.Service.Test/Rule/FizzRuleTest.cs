﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Moq;
using FluentAssertions;
using Aviva_FizzBuzz.Service.Rule;

namespace Aviva_FizzBuzz.Service.Test.Rule
{
    [TestFixture]
    public class FizzRuleTest
    {
        private FizzRule fizzRule;

        [SetUp]
        public void SetUpTests()
        {
            this.fizzRule = new FizzRule();
        }

        [Test]
        public void ModCheck_ShouldReturnTrue_WhenNumberIsDivisibleByThree()
        {
            bool result;

            result = this.fizzRule.ModCheck(3);

            result.Should().Be(true);
        }

        [Test]
        public void Print_ShouldReturnFizz_WhenCalled()
        {
            string output;

            output = this.fizzRule.Print(3, DayOfWeek.Monday);

            output.Should().Be("fizz");
        }

        [Test]
        public void Print_ShouldReturnWizz_WhenDayOfWeekIsWednesday()
        {
            string output;

            output = this.fizzRule.Print(3, DayOfWeek.Wednesday);

            output.Should().Be("wizz");
        }
    }

}
