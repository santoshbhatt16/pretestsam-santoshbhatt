﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Moq;
using FluentAssertions;
using Aviva_FizzBuzz.Service.Rule;
using Aviva_FizzBuzz.Service.Contract;

namespace Aviva_FizzBuzz.Service.Test
{
    [TestFixture]
    public class FizzBuzzServiceTest
    {
        private IFizzBuzzService service;

        private List<IRule> rules;

        private List<string> fizzBuzzDataList = null;

        private Mock<FizzRule> fizzRule = new Mock<FizzRule>();

        private Mock<BuzzRule> buzzRule = new Mock<BuzzRule>();

        Mock<DefaultRule> defaultRule = new Mock<DefaultRule>();

        [SetUp]
        public void SetUpTests()
        {
            this.rules = new List<IRule>();
            this.rules.Add(this.fizzRule.Object);
            this.rules.Add(this.buzzRule.Object);
            this.rules.Add(this.defaultRule.Object);
            this.service = new FizzBuzzService(this.rules);
        }

        [Test]
        public void GetFizzBuzzData_ShouldReturnListCount_EqualToUserInput()
        {
            fizzBuzzDataList = this.service.GetFizzBuzzData(15);

            fizzBuzzDataList.Count.Should().Be(15);
        }

        [Test]
        public void GetFizzBuzzData_ShouldReturnFizz_WhenNumberIsDivisibleByThree()
        {
            this.fizzRule.Setup(m => m.ModCheck(3)).Returns(true);
            this.fizzRule.Setup(m => m.Print(3, It.IsAny<DayOfWeek>())).Returns("fizz");

            this.fizzBuzzDataList = this.service.GetFizzBuzzData(3);

            this.fizzBuzzDataList[2].Should().Be("fizz");
        }

        [Test]
        public void GetFizzBuzzData_ShouldReturnBuzz_WhenNumberIsDivisibleByFive()
        {
            this.buzzRule.Setup(m => m.ModCheck(5)).Returns(true);
            this.buzzRule.Setup(m => m.Print(5, It.IsAny<DayOfWeek>())).Returns("buzz");

            this.fizzBuzzDataList = this.service.GetFizzBuzzData(5);

            this.fizzBuzzDataList[4].Should().Be("buzz");
        }

        [Test]
        public void GetFizzBuzzData_ShouldReturnFizzBuzz_WhenNumberIsDivisibleByThreeAndFive()
        {
            this.fizzRule.Setup(m => m.ModCheck(15)).Returns(true);
            this.fizzRule.Setup(m => m.Print(15, It.IsAny<DayOfWeek>())).Returns("fizz");

            this.buzzRule.Setup(m => m.ModCheck(15)).Returns(true);
            this.buzzRule.Setup(m => m.Print(15, It.IsAny<DayOfWeek>())).Returns("buzz");

            this.fizzBuzzDataList = this.service.GetFizzBuzzData(15);

            this.fizzBuzzDataList[14].Should().BeOneOf("fizz buzz");
        }

        [Test]
        public void GetFizzBuzzData_ShouldReturnIntegerValue_WhenNumberIsNotDivisibleByThreeAndFive()
        {
            this.defaultRule.Setup(m => m.ModCheck(1)).Returns(true);
            this.defaultRule.Setup(m => m.Print(1, It.IsAny<DayOfWeek>())).Returns("1");

            this.fizzBuzzDataList = this.service.GetFizzBuzzData(1);

            this.fizzBuzzDataList[0].Should().Be("1");
        }
    }
}
