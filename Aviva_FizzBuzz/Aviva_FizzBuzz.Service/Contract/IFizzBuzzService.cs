﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aviva_FizzBuzz.Service.Contract
{
    public interface IFizzBuzzService
    {
        List<string> GetFizzBuzzData(int? value);
    }
}
