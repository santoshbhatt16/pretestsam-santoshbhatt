﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aviva_FizzBuzz.Service.Contract
{
    public interface IRule
    {
        bool ModCheck(int value);

        string Print(int value, DayOfWeek dayOfWeek);
    }
}
