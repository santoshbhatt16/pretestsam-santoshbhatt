﻿using Aviva_FizzBuzz.Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aviva_FizzBuzz.Service.Rule
{
    public class BuzzRule : IRule
    {
        public virtual bool ModCheck(int value)
        {
            return value % 5 == 0;
        }

        public virtual string Print(int value, DayOfWeek dayOfWeek)
        {
            return dayOfWeek == DayOfWeek.Wednesday ? "wuzz" : "buzz";
        }
    }
}
