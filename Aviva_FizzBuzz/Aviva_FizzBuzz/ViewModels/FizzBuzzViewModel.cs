﻿using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Aviva_FizzBuzz.ViewModels
{
    public class FizzBuzzViewModel
    {
        [Display(Name = "Enter Number")]
        [Required(ErrorMessage = "Number is mandatory")]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Number must be positive")]
        [Range(1, 1000, ErrorMessage = "Please enter number between 1 to 1000")]
        public int? UserInput { get; set; }

        public IPagedList<string> DisplayData { get; set; }
    }

}