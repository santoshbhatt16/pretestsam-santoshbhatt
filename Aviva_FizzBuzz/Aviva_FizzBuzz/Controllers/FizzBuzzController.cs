﻿using Aviva_FizzBuzz.Service.Contract;
using Aviva_FizzBuzz.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace Aviva_FizzBuzz.Controllers
{
    public class FizzBuzzController : Controller
    {
        private IFizzBuzzService service;

        public FizzBuzzController(IFizzBuzzService service)
        {
            this.service = service;
        }

        //
        // GET: /FizzBuzz/

        public ActionResult FizzBuzzUserInput()
        {
            return View(viewName: "FizzBuzz");
        }

        // GET: /GetFizzBuzzList/
        public ActionResult GetFizzBuzzList(FizzBuzzViewModel fizzBuzzViewModel, int? page = 1)
        {
            if (ModelState.IsValid)
            {
                int pageSize = 20;
                int pageNumber = page ?? 1;

                fizzBuzzViewModel.DisplayData = this.service.GetFizzBuzzData(fizzBuzzViewModel.UserInput).ToPagedList(pageNumber, pageSize);
                return this.View("FizzBuzz", fizzBuzzViewModel);
            }
            else
            {
                return this.View("FizzBuzz");
            }

        }

    }
}
